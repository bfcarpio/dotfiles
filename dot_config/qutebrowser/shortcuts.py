config.bind(";h", "set downloads.location.directory ~ ;; hint links download")
config.bind(";d", "set downloads.location.directory ~/Documents ;; hint links download")
config.bind(";D", "set downloads.location.directory ~/Downloads ;; hint links download")
config.bind(";pp", "set downloads.location.directory ~/Pictures ;; hint links download")
config.bind(";vv", "set downloads.location.directory ~/Videos ;; hint links download")
config.bind(";mm", "set downloads.location.directory ~/Music ;; hint links download")
config.bind(";b", "set downloads.location.directory ~/Books ;; hint links download")
config.bind(";s", "set downloads.location.directory ~/.config/Scripts ;; hint links download")
config.bind(";r", "set downloads.location.directory / ;; hint links download")
config.bind(";cf", "set downloads.location.directory ~/.config ;; hint links download")
config.bind(";Liason", "set downloads.location.directory /media/Liason ;; hint links download")
config.bind(";msu", "set downloads.location.directory ~/Documents/MSU ;; hint links download")
config.bind(";code", "set downloads.location.directory ~/Documents/CodeProj ;; hint links download")
