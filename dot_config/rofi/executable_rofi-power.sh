#!/usr/bin/env bash

# rofi-power
# Use rofi to call systemctl for shutdown, reboot, etc

# 2016 Oliver Kraitschy - http://okraits.de
# Modified by Brendan Carpio to not  include hibernation and not escape WM
OPTIONS="Lock system\nSuspend system\nPower-off system\nReboot system"

 LAUNCHER="rofi -width 30 -dmenu -i -p rofi-power:"
  USE_LOCKER="true"
  LOCKER="i3lock"

option=`echo -e $OPTIONS | $LAUNCHER | awk '{print $1}' | tr -d '\r\n'`
if [ ${#option} -gt 0 ]
then
    case $option in
      Exit)
        eval $1
        ;;
      Lock)
        ~/.config/i3/lock.sh
        ;;
      Reboot)
        systemctl reboot
        ;;
      Power-off)
        systemctl poweroff
        ;;
      Suspend)
        ~/.config/i3/lock.sh; systemctl suspend
        ;;
     *)
        ;;
    esac
fi
