if &compatible
  set nocompatible
endif
" append to runtime path
set rtp+=/usr/share/vim/vimfiles
" initialize dein, plugins are installed to this directory
call dein#begin(expand('~/.cache/dein'))

" support config files
call dein#add('editorconfig/editorconfig-vim')

" syntax for many languages dynamically
call dein#add('sheerun/vim-polyglot')

" create, change, delete surroundings
call dein#add('tpope/vim-surround')

" repeats plugin commands
call dein#add('tpope/vim-repeat')

" Smart increments for date/times
call dein#add('tpope/vim-speeddating')

" adds closing character after opening
call dein#add('jiangmiao/auto-pairs')

" auto comment based on language with keycommand
call dein#add('tpope/vim-commentary')

" better markdown syntax
call dein#add('gabrielelana/vim-markdown')

call dein#add('nelstrom/vim-visual-star-search')

" creates text object based on indents (Python)
call dein#add('michaeljsmith/vim-indent-object')

" auto closing of (x)html tags
call dein#add('alvan/vim-closetag')

" vim unix commands: move, copy, delete
call dein#add('tpope/vim-eunuch')

" Fancy substitutions, case chaging and more...
call dein#add('tpope/vim-abolish')

" Adds commands to use Dein with
call dein#add('haya14busa/dein-command.vim')

" Git integration
call dein#add('tpope/vim-fugitive')
call dein#add('airblade/vim-gitgutter')

" colorscheme
call dein#add('ayu-theme/ayu-vim')
call dein#add('vim-airline/vim-airline')
call dein#add('vim-airline/vim-airline-themes')

" fuzzy search and ripgrepping
call dein#add('junegunn/fzf.vim')
call dein#add('fszymanski/fzf-gitignore')

" focus mode
call dein#add('junegunn/goyo.vim')
call dein#add('junegunn/limelight.vim')

" formats code on file save
call dein#add('Chiel92/vim-autoformat')

" run tests on code
call dein#add('janko/vim-test')

" Easier Elixir development
call dein#add('mattreduce/vim-mix')

" make writing easier
call dein#add('reedes/vim-pencil')

" the FASTEST colorizer (apparently)
call dein#add('norcalli/nvim-colorizer.lua')

" templates for faster development
call dein#add('tpope/vim-projectionist')

" language servers and completion
call dein#add('neoclide/coc.nvim', {'merged': 0, 'rev': 'release'})

let g:coc_global_extensions = ['coc-python', 'coc-json', 'coc-go', 'coc-snippets', 'coc-tabnine', 'coc-elixir', 'coc-texlab', 'coc-sql', "coc-rls", "coc-tsserver"]

" exit dein
call dein#end()
" auto-install missing packages on startup
if dein#check_install()
  call dein#install()
endif
filetype plugin indent on

" #############
" ### MISC. ###
" #############

" sane exit from terminal mode
:tnoremap <Esc> <C-\><C-n>

" Show numbers on the side
set number
syntax on
set autoindent

set scrolloff=1
set ruler
set autoread
set splitbelow
set splitright

" Change panes with Ctrl and direction
map <C-h> <C-w>h
let g:Ctrl_j = 'off'
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Turn backup off, since most stuff is in SVN, git etc. anyway...
set nobackup
set nowb
set noswapfile

" Highlight search results
set hlsearch
" Incremental search, search as you type
set incsearch
" Ignore case when searching lowercase
set smartcase
" Stop highlighting on Enter
map <CR> :noh<CR>

" colorscheme
set termguicolors
let ayucolor="mirage"
let g:airline_theme='ayu_mirage'
set noshowmode
colorscheme ayu

" #################
" ### Colorizer ###
" #################
lua require'colorizer'.setup()

" ########################
" ### Goyo / Limelight ###
" ########################
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!


" #################
" ### Commenter ###
" #################
" Sublime like keybind to comment
nmap <C-_> gcc
autocmd FileType elixir setlocal commentstring=#\ %s

" ################
" ### Closetag ###
" ################
let g:closetag_filetypes = 'html,xhtml,phtml,eelixir'


" ##################
" ### Autoformat ###
" ##################
au BufWrite * :Autoformat
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 1

let g:formatters_javascript = ['prettier', 'jsbeautify_javascript']
let g:formatters_json = ['prettier', 'jsbeautify_json']
let g:formatters_css = ['prettier', 'cssbeautify']
let g:formatters_typescript = ['prettier', 'tsfmt']
let g:formatters_markdown = ['prettier', 'remark_markdown']


" ####################
" ### Editorconfig ###
" ####################
let g:EditorConfig_exclude_patterns = ['fugitive://.*','scp://.*']


" ###############
" ### FZF.vim ###
" ###############
" Ctrl + P file search
nnoremap <C-p> :Files<cr>
" Jump between buffers
nnoremap gb :Buffers<cr>
" Search file content
nnoremap <C-F> :Rg<cr>


" ###############
" ### COC.vim ###
" ###############

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
" Jump to snippet points
let g:coc_snippet_next = '<tab>'
let g:coc_snippet_prev = '<S-Tab>'

" Use enter to accept snippet expansion
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<CR>"

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
if has('patch8.1.1068')
  " Use `complete_info` if your (Neo)Vim version supports it.
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
" xmap <leader>f  <Plug>(coc-format-selected)
" nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
