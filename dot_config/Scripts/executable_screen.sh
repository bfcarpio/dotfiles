#!/bin/bash

# Taken from /u/mepholic
# IN is the laptop's internal monitor
# EXT is the external monitor
# DIRECTION is the side of the laptop that the external monitor is on.
# When you dock or undock, simply MOD+SHIFT+r
# to cause i3 to restart and run this script. 

# toggles the external monitor on/off in specified direction
IN="eDP1"
EXT="HDMI1"
DIRECTION="left"

if (xrandr | grep "$EXT disconnected"); then
    xrandr --output $IN --auto --output $EXT --off 
else
    xrandr --output $IN --auto --primary --output $EXT --auto --$DIRECTION-of $IN
fi
